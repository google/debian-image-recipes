#!/bin/sh
#
# Downloads the latest artifacts from Collabora GitLab for the ChromeOS Kernel
# image builds.
#

# Refuse to overwrite already existing prebuilt directory
if [ -d "prebuilt" ] ; then
    echo "ERROR: prebuilt directory exists. Please remove it before running this script."
    exit 1
fi

# Download linux artifacts
mkdir -p prebuilt/linux
wget -O prebuilt/linux/linux.zip "https://gitlab.collabora.com/google/chromeos-kernel/-/jobs/artifacts/for-kernelci/download?job=build%20arm64%20debian%20package"
unzip -j prebuilt/linux/linux.zip -d prebuilt/linux/

# List contents
find prebuilt/
