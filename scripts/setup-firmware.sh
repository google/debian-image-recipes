#!/bin/sh

BASE_URL=https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/
TARGET_BASE_DIR=/lib/firmware

FILES="
mediatek/mt8173/vpu_d.bin
mediatek/mt8173/vpu_p.bin
mediatek/mt8183/scp.img
mediatek/mt8186/scp.img
mediatek/mt8192/scp.img
mediatek/mt8195/scp.img
mediatek/sof-tplg/sof-mt8186.tplg
mediatek/sof-tplg/sof-mt8195-mt6359-rt1019-rt5682-dts.tplg
mediatek/sof/sof-mt8186.ri
mediatek/sof/sof-mt8195.ri
mediatek/WIFI_MT7961_patch_mcu_1_2_hdr.bin
mediatek/WIFI_RAM_CODE_MT7961_1.bin
"

for file in $FILES; do
  target_file="$TARGET_BASE_DIR"/"$file"
  target_dir=$(dirname "$target_file")
  [ ! -d "$target_dir" ] && mkdir -p "$target_dir"
  curl "$BASE_URL"/"$file" > "$target_file"
done
