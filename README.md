# debian-image-recipes for Chromebooks

Debos recipes to build Debian images for Chromebooks.

Currently, only the MediaTek based Chromebooks (MT8192-Spherion, MT8195-Tomato,
MT8186-Steelix), are supported by the images.

These images can be flashed to any removable media.

## Prebuilt images
See the [CI/CD pipelines](https://gitlab.collabora.com/nfraprado/debian-image-recipes/-/pipelines)
to download prebuilt images for your target. Extract the archive somewhere.

## Install to removable media
Copy the image to an SD card or USB flash drive using `bmaptool`:
```
$ bmaptool copy image-chromebook-chromebook.img.gz <your-storage-dev-path>
```

## System requirements
On your host system, install Docker:
```bash
$ sudo apt install docker
```

It will be used to retrieve the debos container and run it.

## Locally build images
The first stage is to build a generic (but architecture-specific) ospack, then
assemble the ospack into multiple hardware-specific images.

Linux kernel binaries for your specific platform needs to be copied into the
`prebuilt` directory. See `./download-chromebook-kernel-artifacts.sh` for the
directory layout.


```bash
$ ./download-chromebook-kernel-artifacts.sh
$ mkdir out
$ export DEBOS="sudo docker run --volume /dev/shm --tmpfs /dev/shm:rw,nosuid,nodev,exec -v $(pwd):/data -w /data ghcr.io/nfraprado/debos:latest"
$ $DEBOS --artifactdir=out -t architecture:arm64 -m 6G ospack-debian.yaml
$ $DEBOS --artifactdir=out -t architecture:arm64 -t platform:chromebook -m 6G image-chromebook.yaml
```

## TODO

Use upstream debos once partattrs support lands.
